<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registro</title>
</head>
<body>
    <h1>FORMULARIO DE REGISTRO</h1>
    <form method="POST" action="">
        <table>
            <tr>
                <td>
                    Nombre:
                </td>
                <td>
                    <input type="text" name="nombre">
                </td>
            </tr>
            <tr>
                <td>
                    Apellidos:
                </td>
                <td>
                    <input type="text" name="apellidos">
                </td>
            </tr>
            <tr>
                <td>
                    Teléfono:
                </td>
                <td>
                    <input type="text" name="tel">
                </td>
            </tr>
            <tr>
                <td>
                    Correo:
                </td>
                <td>
                    <input type="text" name="correo">
                </td>
            </tr>
        </table>
        <input type="submit" name="btnRegistrar" value="Registrarme"> <input type="reset">
    </form>
    <?php
    if(isset($_POST['btnRegistrar'])){
        require("registro.php");
    }
    ?>
</body>
</html>